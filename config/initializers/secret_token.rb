# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BlogApp::Application.config.secret_key_base = 'e71e30f4c508895279fc33044a5156371b22c6a08be345c9c2a775bbc3becfc003d4e411b57d72f24f7df1b9ad768ecf49c6eabf074f5c7688b17c9adfaf0ff6'
